import contextlib
import logging
import os
import re
import subprocess
import threading


# TODO logger_pipe() is cargo culted from scap. When make-container-image is
# integrated into scap, use scap.log.pipe.
@contextlib.contextmanager
def logger_pipe(logger=None, level=logging.INFO):
    """
    Yields a write-only file descriptor that forwards lines to the given
    logger.
    """
    if logger is None:
        logger = logging.getLogger()

    (reader, writer) = os.pipe()

    def log_lines(logger, level, fd):
        with os.fdopen(fd) as reader:
            for line in reader:
                logger.log(level, line.rstrip())

    thread = threading.Thread(
        target=log_lines,
        args=(
            logger,
            level,
            reader,
        ),
    )

    thread.start()

    try:
        yield writer
    finally:
        # Note the reader is closed by os.fdopen in log_lines
        os.close(writer)
        thread.join(timeout=10)


def image_name_without_tag(image):
    m = re.match("(.*):.*$", image)
    return image if not m else m[1]


class App:
    """
    App provides a logger and consistent logging of subprocess commands to
    derived app classes.
    """
    def __init__(self, logger=logging.getLogger(__name__)):
        self.logger = logger

    def check_call(self, cmd, *args, stdout=None, stderr=subprocess.STDOUT, **kwargs):
        """
        Executes the given command and by default forwards the process output
        to the application instance's logger.
        """
        self.logger.info("Running %s", " ".join(cmd))
        with logger_pipe(logger=self.logger) as out:
            if stdout is None:
                stdout = out
            return subprocess.check_call(
                cmd,
                *args,
                stdout=stdout,
                stderr=stderr,
                **kwargs,
            )

    def push_image(self, image_ref, tag=None):
        """
        Pushes the given image, either by its current name or under a new tag.
        """

        if tag is not None:
            new_image_ref = image_name_without_tag(image_ref) + ":" + tag
            self.check_call(["docker", "tag", image_ref, new_image_ref])
            image_ref = new_image_ref

        self.logger.info("Pushing %s", image_ref)
        self.check_call(["sudo", "/usr/local/bin/docker-pusher", "-q", image_ref])


class ThreadedApp:
    """
    Provides threading of application instances.
    """

    def __init__(self, app_cls, args=()):
        self.app_cls = app_cls
        self.app_args = args
        self.threads = []

    def run(self, name, args=(), **kwargs):
        """
        Creates a new instance of the application, a new logger for that
        instance, and spawns a thread.

        The thread target's arguments are prepended with the application
        instance.

        The thread is started and will be waited on (along with all other
        threads) when :func:`join` is called.
        """

        app = self.app_cls(*self.app_args, logger=logging.getLogger(name))
        thread = threading.Thread(args=(app, *args), **kwargs)
        thread.start()
        self.threads.append(thread)

    def join(self, **kwargs):
        """
        Waits for all threads created by :func:`run` to finish and clears them
        from its list.
        """

        for thread in self.threads:
            thread.join(**kwargs)

        self.threads.clear()
