#!/usr/bin/env python3

# This script is used by
# https://releases-jenkins.wikimedia.org/job/Automatic%20branch%20cut

import argparse
import os
import requests
import subprocess
import sys

def cleanup_cruft():
   # Clean up from prior attempts to branch
   subprocess.run("rm -fr /tmp/mw-branching-*", check=True, shell=True)


def run_branch_py(args, noop):
   cmd = [ "./branch.py" ]

   if noop:
      cmd.append("--noop")

   cmd = cmd + args

   print("Running: {}".format(" ".join(cmd)))
   sys.stdout.flush()

   subprocess.run(cmd, check=True, cwd="make-release")


def do_branch_cut(noop):
   r=requests.get("https://train-blockers.toolforge.org/api.php")
   r.raise_for_status()
   info=r.json()

   version=info["current"]["version"]
   status=info["current"]["status"]
   task=info["current"]["task_id"]

   # Sanity check
   if not version:
      raise SystemExit("version is blank.  This should not happen!")

   if status != "open":
      print(f"Phabricator task {task} has status '{status}'.  Cancelling operation.")
      sys.exit(0)

   print(f"Branching mediawiki version {version} ({task})")

   cleanup_cruft()

   run_branch_py(["--core", "--core-bundle", "wmf_core",
                  "--bundle", "wmf_branch",
                  "--branchpoint", "HEAD",
                  "--core-version", version,
                  "--push-option", "l=Code-Review+2",
                  "--task", task,
                  f"wmf/{version}" ],
                 noop)


def test_branch_cut(noop):
   # Clean up from the last run
   run_branch_py(["--delete", "--skip-tag", "--abandon",
                  "--core", "--bundle", "wmf_branch",
                  "wmf/branch_cut_pretest"],
                 noop)
   # Make the test branch
   run_branch_py(["--core", "--core-bundle", "wmf_core",
                  "--bundle", "wmf_branch",
                  "--branchpoint", "HEAD", "--core-version", "keep",
                  "--push-option", "l=Code-Review+2",
                  "wmf/branch_cut_pretest"],
                 noop)


def main():
   ap = argparse.ArgumentParser()
   ap.add_argument('--test', action='store_true', help='(Re)Create the wmf/branch_cut_pretest branch')
   ap.add_argument('--noop', action='store_true', help='Enable no-op mode which explains what would be done')
   args = ap.parse_args()

   if args.test:
      test_branch_cut(args.noop)
   else:
      do_branch_cut(args.noop)


if __name__ == "__main__":
   main()
